from mongoengine import *

class MongoPhotographer(Document):
    interests = ListField(StringField(max_length=30))
    last_name = StringField(max_length=120, required=True)
    display_name = StringField(max_length=120, required=True)
    first_name = StringField(max_length=120, required=True)


class MongoPhoto(Document):
    title = StringField(max_length=120, required=True)
    last_location = StringField(max_length=120, required=True)
    author = StringField(max_length=120, required=True)
    comment = StringField(max_length=120, required=True)
    tags = ListField(StringField(max_length=30))

class Pic(Document):
    image_file = ImageField(required=True)

